# AQA Assesment UI Test



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Xera01/aqa-assesment-ui-test.git
git branch -M main
git push -uf origin main
```
# Cypress UI Test Project

This repository contains UI tests for the Sauce demo application using Cypress.

## Installation

1. Clone the repository:

```bash
git clone <repository_url>

## Install dependencies
npm install

##Running tests
npm run cypress:open

npm test

## Test Structure
The tests are organized into folders based on functionality. Each folder contains one or more test specs related to that functionality.

## Cypress Configuration
Cypress configuration settings can be found in the cypress.json file. Here you can configure options such as the base URL, test retries, and more.

## CI/CD Pipeline
This project is configured to run on a CI/CD pipeline on GitLab. The .gitlab-ci.yml file defines the pipeline stages and jobs, including test execution.

## Reporting
Test results and artifacts (such as screenshots and videos) are automatically generated and stored as artifacts in the CI/CD pipeline. You can access them in the pipeline details on GitLab.

## Contributing
Contributions are welcome! If you find any issues or have suggestions for improvements, please open an issue or create a pull request.




