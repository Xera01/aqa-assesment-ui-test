describe('SauceDemo Sorting Test', () => {
  it('should verify sorting functionality', () => {
    // Step 1: Visit the SauceDemo website
    cy.visit('https://www.saucedemo.com/');

    // Step 2: Log in to the site
    cy.get('#user-name').click();
    cy.get('#user-name').type('standard_user');
    cy.get('#password').click();
    cy.get('#password').type('secret_sauce');
    cy.get('#login-button').click();

    // Step 3: Verify that the items are sorted by Name (A -> Z)
    cy.get('.product_sort_container').select('az');
    cy.get('.inventory_item_name').then($items => {
      const itemNames = $items.map((index, element) => Cypress.$(element).text()).get();
      expect(itemNames).to.eql(itemNames.slice().sort());
    });

    // Step 4: Change sorting to Name (Z -> A) and verify
    cy.get('.product_sort_container').select('za');
    cy.get('.inventory_item_name').then($items => {
      const itemNames = $items.map((index, element) => Cypress.$(element).text()).get();
      expect(itemNames).to.eql(itemNames.slice().sort().reverse());
    });
  });
});