describe('Sorting', () => {
    it('Login scenario Passed', () => {
      cy.visit('https://www.saucedemo.com/')
      cy.get('[data-test=username]').type('standard_user');
      cy.get('[data-test=password]').type('secret_sauce');
      cy.get('.login_wrapper').click();
      cy.get('[data-test=login-button]').click();
  
      cy.get('#react-burger-menu-btn').click();
  
      cy.get('#logout_sidebar_link').click();
  
      
       });


    it('Verify that the items are sorted by Name ( A -> Z ). ', () => {
        cy.visit('https://www.saucedemo.com/');
        cy.get('[data-test=username]').type('standard_user');
        cy.get('[data-test=password]').type('secret_sauce');
        cy.get('.login_wrapper').click();
        cy.get('[data-test=login-button]').click();
    
    
    
        // Get the list of prices before filtering
       let beforeFilterPriceList = [];
    
        cy.get('.inventory_item_price').each(($price) => {
        const priceText = $price.text().replace('$', '');
        beforeFilterPriceList.push(parseFloat(priceText));
        }).then(() => {
        // Sort the list of prices
        beforeFilterPriceList = beforeFilterPriceList.sort();
    
        // Filter the prices using a dropdown selection
        cy.get('.product_sort_container').select('Price (low to high)');
    
        // Get the list of prices after filtering
        let afterFilterPriceList = [];
    
        cy.get('.inventory_item_price').each(($price) => {
        const priceText = $price.text().replace('$', '');
        afterFilterPriceList.push(parseFloat(priceText));
        }).then(() => {
        // Sort the list of prices
        afterFilterPriceList = afterFilterPriceList.sort();
    
        // Assert that the filtered prices match the original prices
        expect(beforeFilterPriceList).to.deep.equal(afterFilterPriceList);
        });
    });
     
    
    
 })



 it('Change the sorting to Name ( Z -> A). ', () => {
    cy.visit('https://www.saucedemo.com/')
    cy.get('[data-test=username]').type('standard_user');
    cy.get('[data-test=password]').type('secret_sauce');
    cy.get('.login_wrapper').click();
    cy.get('[data-test=login-button]').click();

    //Change the sorting to Name ( Z -> A).
    cy.xpath ("//div[@id=\'header_container\']/div[2]/div/span/select").click();

//before filter capture the prices
const beforeFilterPrice = cy.get('.inventory_item_price');

//remove the $symbol from the price and convert the string into double
const beforeFilterPriceList = [];
beforeFilterPrice.each(($el) => {
beforeFilterPriceList.push(parseFloat($el.text().replace('$', '')));
});

//filter the price from the drop down
cy.get('.product_sort_container').select('Price (high to low)');

//After filter capture the prices
const afterFilterPrice = cy.get('.inventory_item_price');

// remove $ symbol from the price and convert the string into double
const afterFilterPriceList = [];
afterFilterPrice.each(($el) => {
afterFilterPriceList.push(parseFloat($el.text().replace('$', '')));
});

//verify the items are sorted
//Compare the values/Assert the values(first we need to sort the values of before filterPrice)
beforeFilterPriceList.sort().reverse();
expect(beforeFilterPriceList).to.deep.equal(afterFilterPriceList);


  })



});